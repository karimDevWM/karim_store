﻿using Api.karim_store.Business.Service;
using Api.karim_store.Business.Service.Contract;
using Api.karim_store.Datas.Context.Contract;
using Api.karim_store.Datas.Entities;
using Api.karim_store.Datas.Repository;
using Api.karim_store.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.IoC.WebApi
{
    public static class IoCApplication
    {
        /// Configuration de l'injection des repository du Web API RestFul
        public static IServiceCollection ConfigureInjectionDependencyRepository(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Repositories

            services.AddScoped<IRepositoryProduit, RepositoryProduit>();

            return services;
        }


        // Configure l'injection des services

        public static IServiceCollection ConfigureInjectionDependencyService(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Service

            services.AddScoped<IServiceProduit, ServiceProduit>();

            return services;
        }


        //configure connection to db
        public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("BddConnection");

            services.AddDbContext<IKarim_storeContext, karim_storeDBContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors());

            return services;
        }
    }
}
