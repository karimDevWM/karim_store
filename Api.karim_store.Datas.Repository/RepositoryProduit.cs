﻿using Api.karim_store.Datas.Context.Contract;
using Api.karim_store.Datas.Entities.Model;
using Api.karim_store.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.Datas.Repository
{
    public class RepositoryProduit : GenericRepository<Produit>, IRepositoryProduit
    {
        public RepositoryProduit(IKarim_storeContext karim_StoreContext) : base(karim_StoreContext)
        {

        }

        //public async Task<Produit> GetProduitAsync()
    }
}
