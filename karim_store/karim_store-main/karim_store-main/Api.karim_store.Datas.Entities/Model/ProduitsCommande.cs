﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class ProduitsCommande
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? QuantiteCdes { get; set; }
        public int? CommandeId { get; set; }
        public int? ProduitId { get; set; }
    }
}
