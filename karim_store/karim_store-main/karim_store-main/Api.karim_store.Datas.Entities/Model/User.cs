﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class User
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Nom { get; set; }
        public string? Prenom { get; set; }
        public string? AdresseMail { get; set; }
        public string? Genre { get; set; }
        public string? Ddn { get; set; }
        public int? AdresseId { get; set; }
        public int? RoleId { get; set; }
    }
}
