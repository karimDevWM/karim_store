﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Etat
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Libelle { get; set; }
    }
}
