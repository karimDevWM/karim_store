﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Commande
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public DateOnly? DateCommande { get; set; }
        public int? UserId { get; set; }
        public int? EtatId { get; set; }
    }
}
