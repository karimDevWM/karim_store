﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Adresse
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Numero { get; set; }
        public string? Libellé { get; set; }
        public string? CodePostale { get; set; }
        public string? Ville { get; set; }
    }
}
