﻿namespace Api.karim_store.Business.IncomingDTO
{
    public class ProductCreationDto
    {
        public int Id { get; set; }
        public string Reference_Produit { get; set; }
        public string Designation_Produit { get; set; }
        public string Description_Produit { get; set; }
        public float Prix_Produit { get; set; }
        public int CategorieId_Produit { get; set; }
        public int Qte_Produit { get; set; }
    }
}