﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Api.karim_store.Datas.Entities.Model;
using Api.karim_store.Datas.Context.Contract;

namespace Api.karim_store.Datas.Entities
{
    public partial class karim_storeDBContext : DbContext, IKarim_storeContext
    {
        public karim_storeDBContext()
        {
        }

        public karim_storeDBContext(DbContextOptions<karim_storeDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Adresse> Adresses { get; set; } = null!;
        public virtual DbSet<Categorie> Categories { get; set; } = null!;
        public virtual DbSet<Commande> Commandes { get; set; } = null!;
        public virtual DbSet<Etat> Etats { get; set; } = null!;
        public virtual DbSet<Produit> Produits { get; set; } = null!;
        public virtual DbSet<ProduitsCommande> ProduitsCommandes { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<Utilisateur> Utilisateurs { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseMySql("server=localhost;database=karim_store;uid=root;pwd=root", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.31-mysql"));
//            }

            optionsBuilder.EnableSensitiveDataLogging();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_general_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<Adresse>(entity =>
            {
                entity.ToTable("adresse");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.CodePostale)
                    .HasMaxLength(255)
                    .HasColumnName("code_postale");

                entity.Property(e => e.Libellé)
                    .HasMaxLength(255)
                    .HasColumnName("libellé");

                entity.Property(e => e.Numero)
                    .HasMaxLength(255)
                    .HasColumnName("numero");

                entity.Property(e => e.Ville)
                    .HasMaxLength(255)
                    .HasColumnName("ville");
            });

            modelBuilder.Entity<Categorie>(entity =>
            {
                entity.ToTable("categorie");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.Label)
                    .HasMaxLength(255)
                    .HasColumnName("label");
            });

            modelBuilder.Entity<Commande>(entity =>
            {
                entity.ToTable("commandes");

                entity.HasIndex(e => e.EtatId, "etat_id");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.DateCommande).HasColumnName("date_commande");

                entity.Property(e => e.EtatId)
                    .HasColumnType("int(11)")
                    .HasColumnName("etat_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("int(11)")
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<Etat>(entity =>
            {
                entity.ToTable("etat");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.Libelle)
                    .HasMaxLength(255)
                    .HasColumnName("libelle");
            });

            modelBuilder.Entity<Produit>(entity =>
            {
                entity.ToTable("produit");

                entity.HasIndex(e => e.CategorieId, "categorie_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.CategorieId)
                    .HasColumnType("int(11)")
                    .HasColumnName("categorie_id");

                entity.Property(e => e.Description)
                    .HasColumnType("text")
                    .HasColumnName("description");

                entity.Property(e => e.Designation)
                    .HasMaxLength(255)
                    .HasColumnName("designation");

                entity.Property(e => e.Prix).HasColumnName("prix");

                entity.Property(e => e.Qte)
                    .HasColumnType("int(11)")
                    .HasColumnName("qte");

                entity.Property(e => e.Reference)
                    .HasMaxLength(255)
                    .HasColumnName("reference");
            });

            modelBuilder.Entity<ProduitsCommande>(entity =>
            {
                entity.ToTable("produits_commandes");

                entity.HasIndex(e => e.CommandeId, "commande_id");

                entity.HasIndex(e => e.ProduitId, "produit_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.CommandeId)
                    .HasColumnType("int(11)")
                    .HasColumnName("commande_id");

                entity.Property(e => e.ProduitId)
                    .HasColumnType("int(11)")
                    .HasColumnName("produit_id");

                entity.Property(e => e.QuantiteCdes)
                    .HasMaxLength(255)
                    .HasColumnName("quantite_cdes");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.Titre)
                    .HasMaxLength(255)
                    .HasColumnName("titre");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.AdresseId, "adresse_id");

                entity.HasIndex(e => e.RoleId, "role_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.AdresseId)
                    .HasColumnType("int(11)")
                    .HasColumnName("adresse_id");

                entity.Property(e => e.AdresseMail)
                    .HasColumnType("text")
                    .HasColumnName("adresse_mail");

                entity.Property(e => e.Ddn)
                    .HasMaxLength(255)
                    .HasColumnName("ddn");

                entity.Property(e => e.Genre)
                    .HasMaxLength(255)
                    .HasColumnName("genre");

                entity.Property(e => e.Nom)
                    .HasMaxLength(255)
                    .HasColumnName("nom");

                entity.Property(e => e.Prenom)
                    .HasMaxLength(255)
                    .HasColumnName("prenom");

                entity.Property(e => e.RoleId)
                    .HasColumnType("int(11)")
                    .HasColumnName("role_id");
            });

            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.ToTable("utilisateurs");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id")
                    .HasComment("Primary Key");

                entity.Property(e => e.AdresseId)
                    .HasColumnType("int(11)")
                    .HasColumnName("adresse_id");

                entity.Property(e => e.AdresseMail)
                    .HasColumnType("text")
                    .HasColumnName("adresse_mail");

                entity.Property(e => e.Ddn)
                    .HasMaxLength(255)
                    .HasColumnName("ddn");

                entity.Property(e => e.Genre)
                    .HasMaxLength(255)
                    .HasColumnName("genre");

                entity.Property(e => e.Nom)
                    .HasMaxLength(255)
                    .HasColumnName("nom");

                entity.Property(e => e.Prenom)
                    .HasMaxLength(255)
                    .HasColumnName("prenom");

                entity.Property(e => e.RoleId)
                    .HasColumnType("int(11)")
                    .HasColumnName("role_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
