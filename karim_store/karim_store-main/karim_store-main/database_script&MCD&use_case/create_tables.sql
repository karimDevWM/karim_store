DROP TABLE IF EXISTS categorie;
CREATE TABLE categorie(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    label VARCHAR(255)
) COMMENT '';


DROP TABLE IF EXISTS produit;
CREATE TABLE produit(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    reference VARCHAR(255),
    designation VARCHAR(255),
    description text,
    prix FLOAT,
    categorie_id int,
    qte int
) COMMENT '';


DROP TABLE IF EXISTS role;
CREATE TABLE role(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    titre VARCHAR(255)
) COMMENT '';


DROP TABLE IF EXISTS adresse;
CREATE TABLE adresse(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    numero VARCHAR(255),
    libellé VARCHAR(255),
    code_postale VARCHAR(255),
    ville VARCHAR(255)
) COMMENT '';


DROP TABLE IF EXISTS utilisateurs;
CREATE TABLE utilisateurs(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    nom VARCHAR(255),
    prenom VARCHAR(255),
    adresse_mail text,
    genre VARCHAR(255),
    ddn VARCHAR(255),
    adresse_id int,
    role_id int
) COMMENT '';


DROP TABLE IF EXISTS commandes;
CREATE TABLE commandes(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    date_commande VARCHAR(255),
    user_id int,
    etat_id int
) COMMENT '';


DROP TABLE IF EXISTS etat;
CREATE TABLE etat(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    libelle VARCHAR(255)
) COMMENT '';


DROP TABLE IF EXISTS produits_commandes;
CREATE TABLE produits_commandes(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    quantite_cdes VARCHAR(255),
    commande_id int,
    produit_id int
) COMMENT '';


ALTER Table produit
ADD Foreign Key (categorie_id) REFERENCES categorie(id);


ALTER Table users
ADD Foreign Key (adresse_id) REFERENCES adresse(id),
ADD Foreign Key (role_id) REFERENCES role(id);


ALTER Table commandes
ADD Foreign Key (user_id) REFERENCES utilisateurs(id),
ADD Foreign Key (etat_id) REFERENCES etat(id);


ALTER Table produits_commandes
ADD Foreign Key (commande_id) REFERENCES commandes(id),
ADD Foreign Key (produit_id) REFERENCES produit(id);