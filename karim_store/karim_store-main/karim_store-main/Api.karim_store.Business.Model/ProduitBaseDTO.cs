﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.Business.Model
{
    public class ProduitBaseDTO
    {
        public string Reference_Produit { get; set; }
        public string Designation_Produit { get; set; }
        public string Description_Produit { get; set; }
        public float Prix_Produit { get; set; }
        public int CategorieId_Produit { get; set; }
        public int Qte_Produit { get; set; }
    }
}
