-- Active: 1672651624498@@127.0.0.1@3306@karim_store
DROP PROCEDURE IF EXISTS fill_table_utilisateurs;

TRUNCATE TABLE utilisateurs 

DELIMITER $$

CREATE PROCEDURE fill_table_utilisateurs(IN utilisateurs_nb INT)
BEGIN
    DECLARE loop_count INT DEFAULT 1;
    DECLARE id INT;
    DECLARE table_count INTEGER;

    DECLARE finished INT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

    TRUNCATE utilisateurs;

    WHILE loop_count <= utilisateurs_nb DO
        INSERT INTO utilisateurs (
            nom, 
            prenom,
            adresse_mail,
            genre,
            ddn,
            adresse_id,
            role_id
        )
        VALUES(
            CONCAT(loop_count,'er'), 
            CONCAT('utilisateur'),
            CONCAT('utilisateur',loop_count,'@gmail.com'),
            SELECT if(id%2==0, 'M', 'F') FROM utilisateurs,
            
        );
        SET loop_count = loop_count+1;
    END WHILE;

END $$

DELIMITER ;