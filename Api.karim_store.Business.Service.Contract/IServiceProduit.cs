﻿using Api.karim_store.Business.IncomingDTO;

namespace Api.karim_store.Business.Service.Contract
{
    public interface IServiceProduit
    {
        //create Produit
        Task<ProductCreationDto> CreateProduitAsync(ProductCreationDto creationProduct);

        //read Produit by id
        //Task<ProduitReadDTO> GetProduitByIdAsync(int produitId);

        ////read all produit
        //Task<List<ProduitReadDTO>> GetProduitsdAsync(ProduitReadDTO produitToRead);

    }
}
