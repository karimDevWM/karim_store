﻿using Api.karim_store.Datas.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.Datas.Repository.Contract
{
    public interface IRepositoryProduit : IGenericRepository<Produit>
    {
        //Task<Produit> GetProduit()
    }
}
