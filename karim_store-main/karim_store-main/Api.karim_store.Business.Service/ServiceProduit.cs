﻿using Api.karim_store.Business.IncomingDTO;
using Api.karim_store.Business.Service.Contract;
using Api.karim_store.Datas.Entities.Model;
using Api.karim_store.Datas.Repository.Contract;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.Business.Service
{
    public class ServiceProduit : IServiceProduit
    {
        private readonly IRepositoryProduit _repositoryProduit;
        private readonly IMapper _mapperProfile;

        public ServiceProduit(IRepositoryProduit repositoryProduit, IMapper mapperProfile)
        {
            _repositoryProduit = repositoryProduit;
            _mapperProfile = mapperProfile;
        }

        public async Task<ProductCreationDto> CreateProduitAsync(ProductCreationDto creationProduct)
        {

            var produit = _mapperProfile.Map<Produit>(creationProduct);

            var product_repository= await _repositoryProduit.CreateElementAsync(produit);

            return _mapperProfile.Map<ProductCreationDto>(product_repository);
        }


        //public Task<ProductCreationDto> CreateProduitAsync(ProductCreationDto produitToAdd)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<ProduitReadDTO> GetProduitByIdAsync(int produitId)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<List<ProduitReadDTO>> GetProduitsdAsync(ProduitReadDTO produitToRead)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
