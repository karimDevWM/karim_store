﻿using Api.karim_store.Datas.Entities.Model;
using Api.simplon_academy.Datas.Context.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.karim_store.Datas.Context.Contract
{
    public interface IKarim_storeContext : IDbContext
    {
        DbSet<Adresse> Adresses { get; set; }
        DbSet<Categorie> Categories { get; set; }
        DbSet<Commande> Commandes { get; set; }
        DbSet<Etat> Etats { get; set; }
        DbSet<Produit> Produits { get; set; }
        DbSet<ProduitsCommande> ProduitsCommandes { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Utilisateur> Utilisateurs { get; set; }
    }
}
