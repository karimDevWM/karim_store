﻿using Api.karim_store.Business.IncomingDTO;
using Api.karim_store.Datas.Entities.Model;
using AutoMapper;

namespace Api.karim_store.Profiles
{
    public class MappertProfile : Profile
    {
        public MappertProfile()
        {
            CreateMap<ProductCreationDto, Produit>()
                .ForMember(
                    dest => dest.Reference,
                    opt => opt.MapFrom(src => src.Reference_Produit)
                )
                .ForMember(
                    dest => dest.Designation,
                    opt => opt.MapFrom(src => src.Designation_Produit)
                )
                .ForMember(
                    dest => dest.Description,
                    opt => opt.MapFrom(src => src.Description_Produit)
                )
                .ForMember(
                    dest => dest.Prix,
                    opt => opt.MapFrom(src => src.Prix_Produit)
                )
                .ForMember(
                    dest => dest.CategorieId,
                    opt => opt.MapFrom(src => src.CategorieId_Produit)
                )
                .ForMember(
                    dest => dest.Qte,
                    opt => opt.MapFrom(src => src.Qte_Produit)
                );
        }
    }
}