﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Produit
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Reference { get; set; }
        public string? Designation { get; set; }
        public string? Description { get; set; }
        public float? Prix { get; set; }
        public int? CategorieId { get; set; }
        public int? Qte { get; set; }
    }
}
