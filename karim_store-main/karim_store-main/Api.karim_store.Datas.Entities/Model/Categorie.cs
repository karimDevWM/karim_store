﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Categorie
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Label { get; set; }
    }
}
