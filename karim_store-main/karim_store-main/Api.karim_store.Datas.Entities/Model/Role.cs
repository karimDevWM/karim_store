﻿using System;
using System.Collections.Generic;

namespace Api.karim_store.Datas.Entities.Model
{
    public partial class Role
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        public string? Titre { get; set; }
    }
}
