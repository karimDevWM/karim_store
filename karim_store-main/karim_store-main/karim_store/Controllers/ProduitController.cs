﻿using Api.karim_store.Business.IncomingDTO;
using Api.karim_store.Business.Service;
using Api.karim_store.Business.Service.Contract;
using Api.karim_store.Datas.Entities.Model;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace karim_store.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProduitController : ControllerBase
    {
        private static List<Produit> produits = new List<Produit>();
        private readonly IServiceProduit _serviceProduit;
        private readonly IMapper _mapper;

        public ProduitController(IServiceProduit serviceProduit, IMapper mapper)
        {
            _serviceProduit = serviceProduit;
            _mapper = mapper;
        }


        [HttpPost()]
        [ProducesResponseType(typeof(Produit), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CreateProductAsync(ProductCreationDto produit)
        {
            var productService = await _serviceProduit.CreateProduitAsync(produit).ConfigureAwait(false);
            Console.WriteLine("product service = ", productService);
            var product = _mapper.Map<Produit>(productService);

            produits.Add(product);

            return Ok(product);
        }

        //// GET: api/<ProduitController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<ProduitController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<ProduitController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<ProduitController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<ProduitController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
