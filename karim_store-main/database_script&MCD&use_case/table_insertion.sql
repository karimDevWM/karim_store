-- Active: 1672651624498@@127.0.0.1@3306@karim_store


insert into 
  adresse ( 
    numero, 
    libellé, 
    code_postale, 
    ville
  )
values
  (
    '13', 
    'chemin des pommes', 
    '69500', 
    'Bron'
  ),
values
  (
    '45', 
    'chemin des poires', 
    '69800', 
    'Saint_Priest'
  ),
values
  (
    '13', 
    'chemin des pasteques', 
    '69100', 
    'Villeurbanne'
  );


insert into 
  role ( 
    titre
  )
values
  (
    'admin'
  ),
values
  (
    'client'
  );


insert into 
  utilisateurs ( 
    nom, 
    prenom, 
    adresse_mail, 
    genre, 
    ddn, 
    adresse_id, 
    role_id
  )
values
  (
    'John', 
    'Doe', 
    'john.doe@gmail.com', 
    'M', 
    '26-10-1950', 
    1, 
    1
  ),
values
  (
    'Jane', 
    'Doe', 
    'jane.doe@gmail.com', 
    'F',
    '17-06-1975',
    2,
    2
  ),
values
  (
    'Jack', 
    'Doe', 
    'jack.doe@gmail.com', 
    'M',
    '26-10-1980',
    3,
    2
  );


insert into 
  categorie ( 
    label
  )
values
  (
    'electromenager'
  ),
values
  (
    'informatique'
  ),
values
  (
    'multimedia'
  );


  insert into 
  produit ( 
    reference, 
    designation, 
    description, 
    prix, 
    categorie_id, 
    qte
  )
values
  (
    "FRIGESAM001",
    "Frigidaire Samsung",
    "description frigidaire Samsung",
    580.99,
    1,
    45
  ),
values
  (
    "TVSAM001",
    "Télévisuer Samsung",
    "description téléviseur Samsung",
    800.99,
    3,
    80
  ),
values
  (
    "ORDISAM001",
    "Ordinateur Samsung",
    "description ordinateur Samsung",
    580.99,
    2,
    45
  );


  insert into 
  etat ( 
    etat
  )
values
  (
    'confirmé'
  ),
values
  (
    'en attente'
  ),
values
  (
    'livré'
  );


  insert into 
  commandes ( 
    date_commande,
    user_id,
    etat_id
  )
values
  (
    '17-03-2015',
    2,
    1
  ),
values
  (
    '17-07-2015',
    3,
    2
  ),
values
  (
    '17-07-2015',
    2,
    3
  );


  insert into 
  produits_commandes ( 
    quantite_cdes,
    commande_id,
    produit_id
  )
values
  (
    4,
    2,
    1
  ),
values
  (
    10,
    3,
    2
  ),
values
  (
    20,
    1,
    3
  );